package cz.jm.paymenttracker.view;

import cz.jm.paymenttracker.controller.RecordsController;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Observable;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Listenes for user input and sends the inpout to the controller
 *
 * @author Jakub Macoun
 */
@Component
public class InputListener extends Observable {

    @Autowired
    private RecordsController controller;

    @PostConstruct
    private void init() {
        addObserver(controller);
    }

    public void startListening() throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        
        while (true) {
            // Console
            final String input = br.readLine();
            
            // Notify observers
            setChanged();
            notifyObservers(input);
        }
    }
}
