package cz.jm.paymenttracker.view;

import cz.jm.paymenttracker.model.dao.core.DAO;
import cz.jm.paymenttracker.model.data.Payment;
import cz.jm.paymenttracker.model.data.UsdExchangeRate;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Singleton managing data output once per minute
 *
 * @author Jakub Macoun
 */
@Component
public class DataOutputWriter {

    public static long INTERVAL_SECONDS = 10L;
    
    private static final String DIVIDER = "---------";

    @Autowired
    private DAO<Payment> dao;

    @Autowired
    private DAO<UsdExchangeRate> rateDao;

    private OutputingThread thread;

    public void startWriting() {
        writeGreeting();
        
        thread = new OutputingThread();
        thread.start();
    }

    public OutputingThread getThread() {
        return thread;
    }
    
    private void writeGreeting() {
        for (int i = 0; i < 3; i++) {
            System.out.println("===============================");
        }
        System.out.println("Welcome to the Payment Tracker!");
    }

    public class OutputingThread extends Thread {

        @Override
        public void run() {
            while (true) {
                try {
                    Collection<Payment> payments = dao.retreiveAll();
                    
                    System.out.println(DIVIDER);
                    for (Payment payment : payments) {
                        if (payment.isVisible()) {
                            String toPrint = payment.toString();

                            UsdExchangeRate rate = rateDao.retreive(payment.getCurrencyCode());
                            if (!payment.getCurrencyCode().equalsIgnoreCase("USD") && rate != null) {
                                BigDecimal inUsd = payment.getAmount().multiply(new BigDecimal(rate.getRate())).setScale(2, RoundingMode.HALF_UP);
                                String inUsdString
                                        = inUsd.toPlainString()
                                        .replaceAll("\\.0+$", "");

                                toPrint += " (USD " + inUsdString + ")";
                            }

                            System.out.println(toPrint);
                        }
                    }
                    System.out.println(DIVIDER);

                    Thread.sleep(INTERVAL_SECONDS * 1000L);
                } catch (InterruptedException ex) {
                }
            }
        }
    }
}
