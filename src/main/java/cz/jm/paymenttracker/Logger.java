package cz.jm.paymenttracker;

import org.apache.logging.log4j.LogManager;

/**
 *
 * @author Jakub Macoun
 */
public class Logger {

    private static final Logger INSTANCE = new Logger();
    
    private static final org.apache.logging.log4j.Logger ERROR = LogManager.getLogger("error");
    
    private Logger() {}
    
    public static Logger getInstance() {
        return INSTANCE;
    }

    public org.apache.logging.log4j.Logger getError() {
        return ERROR;
    }
}
