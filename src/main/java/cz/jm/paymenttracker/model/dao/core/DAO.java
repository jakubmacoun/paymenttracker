package cz.jm.paymenttracker.model.dao.core;

import java.io.Serializable;
import java.util.Collection;

/**
 * Basic DAO interface
 *
 * @author Jakub Macoun
 * @param <T> Type of handled entity
 */
public interface DAO<T extends Entity> {

    // Create
    public void create(T entity);

    // Retreive
    public T retreive(Serializable key);
    public Collection<T> retreiveAll();

    // Update
    public void update(T entity);

    // Delete
    public void delete(T entity);
}
