package cz.jm.paymenttracker.model.dao;

import cz.jm.paymenttracker.Logger;
import cz.jm.paymenttracker.PaymentTracker;
import cz.jm.paymenttracker.model.exception.DatabaseErrorException;
import cz.jm.paymenttracker.model.dao.core.DAO;
import cz.jm.paymenttracker.model.data.Payment;
import cz.jm.paymenttracker.model.exception.DataNotFoundException;
import cz.jm.paymenttracker.model.exception.DuplicateEntryException;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;
import org.springframework.stereotype.Component;

/**
 * DAO for {@link Payment} instances
 *
 * @author Jakub Macoun
 */
@Component
public class PaymentDao implements DAO<Payment> {

    @Override
    public void create(Payment payment) {
        // Check if paymnet with given currency doesn't already exist and prepare new content
        Collection<Payment> payments = retreiveAll();
        StringBuilder newContentBuilder = new StringBuilder();

        for (Payment storedPayment : payments) {
            if (storedPayment.equals(payment)) {
                throw new DuplicateEntryException("Record of currency " + payment.getCurrencyCode() + " already exists");
            } else {
                newContentBuilder.append(String.format(storedPayment.toString().concat("%n")));
            }
        }

        // Create it
        newContentBuilder.append(payment.toString());
        rewiteDatabase(newContentBuilder.toString());
    }

    @Override
    public Payment retreive(final Serializable key) {
        try (Stream<String> stream = obtainStream()) {
            if (stream != null) {
                Optional<String> t = stream.filter(line -> {
                    Payment payment = Payment.parse(line);
                    return payment.getKey().equals(key);
                }).findFirst();

                if (t.isPresent()) {
                    return Payment.parse(t.get());
                }
            }
        }

        return null;
    }

    @Override
    public void update(Payment payment) {
        // Prepare
        Collection<Payment> payments = retreiveAll();
        StringBuilder newContentBuilder = new StringBuilder();
        boolean found = false;

        // Create new content
        for (Payment storedPayment : payments) {
            String toAppend;
            if (storedPayment.equals(payment)) {
                storedPayment.setAmount(storedPayment.getAmount().add(payment.getAmount()));
                toAppend = storedPayment.toString();
                found = true;
            } else {
                toAppend = storedPayment.toString();
            }
            newContentBuilder.append(String.format(toAppend.concat("%n")));
        }

        // Store it
        if (found) {
            rewiteDatabase(newContentBuilder.toString());
        } else {
            throw new DataNotFoundException("No payment for currency " + payment.getCurrencyCode() + " exists");
        }
    }

    @Override
    public void delete(Payment payment) {
        // Prepare
        Collection<Payment> payments = retreiveAll();
        StringBuilder newContentBuilder = new StringBuilder();
        boolean found = false;

        // Create new content
        for (Payment storedPayment : payments) {
            String toAppend;
            if (storedPayment.equals(payment)) {
                found = true;
                continue;
            } else {
                toAppend = storedPayment.toString();
            }
            newContentBuilder.append(String.format(toAppend.concat("%n")));
        }

        // Store it
        if (found) {
            rewiteDatabase(newContentBuilder.toString());
        } else {
            throw new DataNotFoundException();
        }
    }

    @Override
    public Collection<Payment> retreiveAll() {
        final List<Payment> result = new ArrayList<>();

        try (Stream<String> stream = obtainStream()) {
            if (stream != null) {
                stream.forEach(line -> {
                    Payment payment = Payment.parse(line);
                    result.add(payment);
                });
            }
        }

        return result;
    }

    private Stream<String> obtainStream() {
        synchronized (this) {
            try {
                testFileExistence();
                return Files.lines(Paths.get(PaymentTracker.getFilePath()));
            } catch (IOException ex) {
                handleDbError(ex);
                return null;
            }
        }
    }

    private void rewiteDatabase(String newContent) {
        synchronized (this) {
            try {
                testFileExistence();
                Files.write(Paths.get(PaymentTracker.getFilePath()), newContent.getBytes(Charset.forName("UTF-8")));
            } catch (IOException ex) {
                handleDbError(ex);
            }
        }
    }

    private void handleDbError(Exception ex) {
        final String message = "Access to database failed while wokring with payments";

        Logger.getInstance().getError().error(message, ex);
        throw new DatabaseErrorException(message, ex);
    }

    private void testFileExistence() {
        File file = new File(PaymentTracker.getFilePath());
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException ex) {
                System.out.println("Given database file could not be created. Exiting...");
                System.exit(2);
            }
        }
    }
}
