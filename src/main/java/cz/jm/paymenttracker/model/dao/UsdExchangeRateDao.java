package cz.jm.paymenttracker.model.dao;

import cz.jm.paymenttracker.model.dao.core.DAO;
import cz.jm.paymenttracker.model.data.UsdExchangeRate;
import cz.jm.paymenttracker.model.exception.DataNotFoundException;
import cz.jm.paymenttracker.model.exception.DuplicateEntryException;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import org.springframework.stereotype.Component;

/**
 * DAO singleton for entered USD exchange rates
 *
 * @author Jakub Macoun
 */
@Component
public class UsdExchangeRateDao implements DAO<UsdExchangeRate> {

    private final Map<Serializable, UsdExchangeRate> storage = new HashMap<>();

    @Override
    public void create(UsdExchangeRate rate) {
        UsdExchangeRate storedRate = storage.get(rate.getKey());
        
        if (storedRate != null) {
            throw new DuplicateEntryException("Rate for currency " + rate.getCurrencyCode() + " already exists");
        } else {
            storage.put(rate.getKey(), rate);
        }
    }

    @Override
    public UsdExchangeRate retreive(Serializable key) {
        return storage.get(key);
    }

    @Override
    public Collection<UsdExchangeRate> retreiveAll() {
        return storage.values();
    }

    @Override
    public void update(UsdExchangeRate rate) {
        UsdExchangeRate storedRate = storage.get(rate.getKey());
        
        if (storedRate == null) {
            throw new DataNotFoundException("No rate for currency " + rate.getCurrencyCode() + " found");
        } else {
            storage.put(rate.getKey(), rate);
        }
    }

    @Override
    public void delete(UsdExchangeRate rate) {
        storage.remove(rate.getKey());
    }

}
