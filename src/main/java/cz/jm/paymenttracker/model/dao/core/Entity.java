package cz.jm.paymenttracker.model.dao.core;

import java.io.Serializable;

/**
 *
 * @author Jakub Macoun
 */
public interface Entity {

    public Serializable getKey();
    
    public boolean isVisible();
}
