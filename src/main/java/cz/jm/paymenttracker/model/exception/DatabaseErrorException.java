package cz.jm.paymenttracker.model.exception;

/**
 * Exception thrown in case of database access failure
 *
 * @author Jakub Macoun
 */
public class DatabaseErrorException extends RuntimeException {

    public DatabaseErrorException() {
    }

    public DatabaseErrorException(String message) {
        super(message);
    }

    public DatabaseErrorException(String message, Throwable cause) {
        super(message, cause);
    }

    public DatabaseErrorException(Throwable cause) {
        super(cause);
    }

    public DatabaseErrorException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
