package cz.jm.paymenttracker.model.data;

import cz.jm.paymenttracker.model.dao.core.Entity;
import java.io.Serializable;
import java.util.Objects;

/**
 * Class for instancing a USD exchagne rate
 *
 * @author Jakub Macoun
 */
public class UsdExchangeRate implements Entity {
    
    public static final String PATTERN = "^([a-zA-Z]{3}) ([0-9.]+)$";

    private String currencyCode;
    private double rate;

    public UsdExchangeRate() {
    }

    public UsdExchangeRate(String currencyCode, double rate) {
        this.currencyCode = currencyCode;
        this.rate = rate;
    }
    
    public static UsdExchangeRate parse(String parsable) {
        // Check parameters
        if (parsable == null) {
            throw new NullPointerException("Parsable is null");
        }
        if (!parsable.matches(PATTERN)) {
            throw new IllegalArgumentException("Parsable does not fit to regex " + PATTERN);
        }
        
        // Do the parsing
        String currencyCode = parsable.replaceAll(PATTERN, "$1").toUpperCase();
        double rate = Double.parseDouble(parsable.replaceAll(PATTERN, "$2"));
        
        // Finish
        return new UsdExchangeRate(currencyCode, rate);
    }
    
    public void loadFromOther(UsdExchangeRate other) {
        rate = other.rate;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 37 * hash + Objects.hashCode(this.currencyCode);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UsdExchangeRate other = (UsdExchangeRate) obj;
        if (!Objects.equals(this.currencyCode, other.currencyCode)) {
            return false;
        }
        return true;
    }

    @Override
    public Serializable getKey() {
        return getCurrencyCode();
    }

    @Override
    public boolean isVisible() {
        return true;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }
}
