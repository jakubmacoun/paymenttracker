package cz.jm.paymenttracker.model.data;

import cz.jm.paymenttracker.model.dao.core.Entity;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * Class for instancing a single record in the trackers database
 *
 * @author Jakub Macoun
 */
public class Payment implements Entity {
    
    public static final String PATTERN = "^([a-zA-Z]{3}) ([+\\-0-9.]+)$";

    private String currencyCode;
    private BigDecimal amount;

    public Payment() {
    }

    public Payment(String currencyCode, BigDecimal amount) {
        this.currencyCode = currencyCode;
        this.amount = amount;
    }
    
    public static Payment parse(String parsable) {
        // Check parameters
        if (parsable == null) {
            throw new NullPointerException("Parsable is null");
        }
        if (!parsable.matches(PATTERN)) {
            throw new IllegalArgumentException("Parsable does not fit to regex " + PATTERN);
        }
        
        // Do the parsing
        BigDecimal amount = new BigDecimal(parsable.replaceAll(PATTERN, "$2"));
        String currencyCode = parsable.replaceAll(PATTERN, "$1").toUpperCase();
        
        // Finish
        return new Payment(currencyCode, amount);
    }
    
    public void loadFromOther(Payment other) {
        amount = other.amount;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 31 * hash + Objects.hashCode(this.currencyCode);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Payment other = (Payment) obj;
        if (!Objects.equals(this.currencyCode, other.currencyCode)) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return currencyCode + " " + amount.toString().replaceAll("\\.0+$", "");
    }

    @Override
    public Serializable getKey() {
        return getCurrencyCode();
    }

    @Override
    public boolean isVisible() {
        return !amount.equals(BigDecimal.ZERO);
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
