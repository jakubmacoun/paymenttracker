package cz.jm.paymenttracker.controller;

import cz.jm.paymenttracker.Logger;
import cz.jm.paymenttracker.PaymentTracker;
import cz.jm.paymenttracker.controller.command.core.Command;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;
import javax.annotation.PostConstruct;
import org.reflections.Reflections;
import org.springframework.stereotype.Component;

/**
 * Controller for managing payments
 *
 * @author Jakub Macoun
 */
@Component
public class RecordsController implements Observer {

    private Set<Class<? extends Command>> commandClasses;

    @PostConstruct
    private void init() {
        initCommands();
    }

    @Override
    public void update(Observable observable, Object arg) {
        if (observable instanceof Object && arg instanceof String) {
            String input = (String) arg;
            try {
                // Find command
                boolean found = false;
                for (Class<? extends Command> cmdClass : commandClasses) {
                    Command command = PaymentTracker.getApplicationContext().getBean(cmdClass);
                    if (command.isExecutable(input)) {
                        command.doAction(input);
                        found = true;
                        break;
                    }
                }

                // Inform of unknown command
                if (!found) {
                    System.out.println("Unknown command");
                }
            } catch (Exception ex) {
                System.out.println("There was an error while performing your command. See logs for further information.");
                Logger.getInstance().getError().error("Error while performing command " + input, ex);
            }
        }
    }

    private void initCommands() {
        Reflections reflections = new Reflections("cz.jm.paymenttracker.controller.command");
        commandClasses = reflections.getSubTypesOf(Command.class);
    }
}
