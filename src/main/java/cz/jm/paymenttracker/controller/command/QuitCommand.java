package cz.jm.paymenttracker.controller.command;

import cz.jm.paymenttracker.controller.command.core.Command;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Command for exiting the application
 *
 * @author Jakub Macoun
 */
@Component
@Scope("prototype")
public class QuitCommand implements Command {

    public static final String INPUT = "quit";

    @Override
    public boolean isExecutable(String input) {
        return StringUtils.equalsIgnoreCase(input, INPUT);
    }

    @Override
    public void doAction(String input) throws Exception {
        confirm();
        System.exit(0);
    }

    @Override
    public void confirm() {
        System.out.println("Exiting Payment Tracker. Bye bye.");
    }

}
