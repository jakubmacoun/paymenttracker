package cz.jm.paymenttracker.controller.command;

import cz.jm.paymenttracker.controller.command.core.Command;
import cz.jm.paymenttracker.model.dao.core.DAO;
import cz.jm.paymenttracker.model.data.UsdExchangeRate;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Command for adding new USD exchange rate
 *
 * @author Jakub Macoun
 */
@Component
@Scope("prototype")
public class UsdRateCommand implements Command {
    
    public static final String INPUT = "usdRate";
    
    @Autowired
    private DAO<UsdExchangeRate> dao;

    @Override
    public boolean isExecutable(String input) {
        return input != null && StringUtils.startsWithIgnoreCase(input, INPUT);
    }

    @Override
    public void doAction(String input) throws Exception {
        // Get data
        String dataInput = StringUtils.removeStartIgnoreCase(input, INPUT).trim();
        UsdExchangeRate rate = UsdExchangeRate.parse(dataInput);
        
        // Try to find existing one
        UsdExchangeRate existingRate = dao.retreive(rate.getKey());
        
        // Update if exists, otherwise create new one
        if (existingRate != null) {
            existingRate.loadFromOther(rate);
            dao.update(existingRate);
        } else {
            dao.create(rate);
        }
    }

    @Override
    public void confirm() {
        System.out.println("USD exchange rate set");
    }

}
