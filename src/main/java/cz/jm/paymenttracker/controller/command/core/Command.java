package cz.jm.paymenttracker.controller.command.core;

/**
 * Input command interface
 *
 * @author Jakub Macoun
 */
public interface Command {

    public boolean isExecutable(String input);

    public void doAction(String input) throws Exception;
    
    public void confirm();
}
