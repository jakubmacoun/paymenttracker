package cz.jm.paymenttracker.controller.command;

import cz.jm.paymenttracker.controller.command.core.Command;
import cz.jm.paymenttracker.model.dao.core.DAO;
import cz.jm.paymenttracker.model.data.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Command for manipulation with payments data
 *
 * @author Jakub Macoun
 */
@Component
@Scope("prototype")
public class DataCommand implements Command {
    
    @Autowired
    private DAO<Payment> dao;

    @Override
    public boolean isExecutable(String input) {
        return input != null && input.matches(Payment.PATTERN);
    }

    @Override
    public void doAction(String input) throws Exception {
        Payment newPayment = Payment.parse(input);
        
        // Try to load existing data
        Payment existingPayment = dao.retreive(newPayment.getKey());
        
        // If exists, update, otherwise create new record
        if (existingPayment != null) {
            existingPayment.loadFromOther(newPayment);
            dao.update(newPayment);
        } else {
            dao.create(newPayment);
        }
    }

    @Override
    public void confirm() {
        System.out.println("Payment registered");
    }

}
