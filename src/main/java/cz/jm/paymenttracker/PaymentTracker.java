package cz.jm.paymenttracker;

import cz.jm.paymenttracker.view.DataOutputWriter;
import cz.jm.paymenttracker.view.InputListener;
import java.io.IOException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Main class of the application
 *
 * @author Jakub Macoun
 */
public class PaymentTracker {

    // Constants
    public static final String DATABASE_FILE_ARG = "-dbFileName";
    public static final String LOGS_PATH_ARG = "-logsFolder";
    public static final String DEFAULT_DB_FILE = "C:\\payments.txt";
    public static final String DEFAULT_LOGS_PATH = "C:\\logs\\paymentTracker\\";

    // Application context and arguments
    private static ApplicationContext applicationContext;
    private static String[] args;

    // Chosen file path
    private static String filePath;

    public static final void main(String... args) {
        PaymentTracker.args = args;
        
        initDbFileName();
        initLogsPath();
        
        configureApplicationContext();
        startDataWriter();
        startInputListener();
    }
    
    private static void initDbFileName() {
        String enteredFileName = getArgValue(DATABASE_FILE_ARG);
        filePath = enteredFileName == null ? DEFAULT_DB_FILE : enteredFileName;
    }
    
    private static void initLogsPath() {
        String enteredPath = getArgValue(LOGS_PATH_ARG);
        String pathToSet;
        if (enteredPath == null) {
            pathToSet = DEFAULT_LOGS_PATH;
        } else {
            pathToSet = enteredPath;
            if (!enteredPath.matches("\\$") && !enteredPath.matches("/$")) {
                pathToSet += "\\";
            }
        }

        System.setProperty("logsDirPath", pathToSet);
    }

    private static void configureApplicationContext() {
        applicationContext = new ClassPathXmlApplicationContext("/beans.xml");
    }

    private static void startInputListener() {
        try {
            InputListener inputListener = applicationContext.getBean(InputListener.class);
            inputListener.startListening();
        } catch (IOException ex) {
            System.out.println("User input listener could not be initiated. Existing...");
            System.exit(1);
        }
    }

    private static void startDataWriter() {
        DataOutputWriter writer = applicationContext.getBean(DataOutputWriter.class);
        writer.startWriting();
    }
    
    private static String getArgValue(String arg) {
        boolean found = false;
        for (String enteredArg : args) {
            if (found) {
                return enteredArg;
            }
            
            if (StringUtils.equalsIgnoreCase(enteredArg, arg)) {
                found = true;
            }
        }
        
        return null;
    }

    public static ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    public static String getFilePath() {
        return filePath;
    }
}
